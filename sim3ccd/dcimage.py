#!/usr/bin/env python
# encoding: utf-8

__all__ = ['Image']


class Image:
    @property
    def data(self):
        "The actual Image (cv::Mat)"
        return self._data

    @property
    def timestamp(self):
        "The IEEE bustime when the picture was acquired (microseconds)"
        return self._timestamp
