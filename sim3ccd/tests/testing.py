#!/usr/bin/env python
# encoding: utf-8

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

from sim3ccd import Camera, SynchronizedCameras

if __name__ == '__main__':

    syn_cams = SynchronizedCameras()
    for i in xrange(0, 3):
        cam = Camera(i)
        cam.set('brigthness', 600)
        cam.set('exposure', 500)  # It is setting the shutter!! // 1300
        cam.set('gain', 650)
        cam.set('fps', 30)
        cam.set('frame_width', 640)
        cam.set('frame_height', 480)
        syn_cams.addCamera(cam)
    syn_cams.start()
    syn_cams.sync()
    syn_cams.stop()
    syn_cams.saveImages()
