#!/usr/bin/env python
# encoding: utf-8

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

from sim3ccd import SynchronizedCameras
from time import sleep

if __name__ == '__main__':

    syn_cams = SynchronizedCameras(1)

    syn_cams.start()
    sleep(1)
    syn_cams.stop()
    syn_cams.sync()
