#!/usr/bin/env python
# encoding: utf-8

from camera import CameraThreaded
from threading import Event
import cv2
from time import time

__all__ = ['SynchronizedCameras']


class SynchronizedCameras(object):
    def __init__(self, num_cams=0):
        """ Construct a list of $num_cams of cameras threaded"""
        self._cameras = []
        self._can_grab_event = Event()
        self._can_grab_event.clear()
        for i in xrange(0, num_cams):
            cam = CameraThreaded(self._can_grab_event, i)
            self._cameras.append(cam)

    def stop(self):
        """ Convenient function which closes all the cameras """
        for i in xrange(0, len(self._cameras)):
            self._cameras[i].abort()

    def addCamera(self, camera=None):
        """ Method for adding a new (already configured) camera """
        if camera is not None:
            _cam = CameraThreaded(self._can_grab_event)
            _cam.setCamera(camera)
            self._cameras.append(_cam)

    def start(self):
        """ Method returning a list with the current
            images in all the cameras """
        print "Cameras starting at: %f" % time()
        self._can_grab_event.set()

    def sync(self):
        # 1. Tomo una imagen de cada camara
        # 2. calculo el valor mas alto
        # 3. calculo el error de cada camara con el maximo calculado
        #    ... con ese error ajusto el time to sleep de cada cam
        stamps = map(lambda x: x.retrieve().timestamp, self._cameras)
        max_val = max(stamps)
        for i in xrange(0, len(stamps)):
            self._cameras[i].setTiming((max_val - stamps[i]) / 1.4)

        for cam in self._cameras:
            cam.setTiming(0.0)
            cam.putInDeque()

    def saveImages(self):
        """ Save the current images from all the cameras """
        # images => [ img0, img1, img2, ... ]
        images = map(lambda x: x.retrieve(), self._cameras)
        # fnames => [ 'test0.jpg', 'test1.jpg', 'test2.jpg', ... ]
        fnames = map(lambda x: 'test%s.jpg' % (x),
                     xrange(0, len(self._cameras)))
        for i in xrange(0, len(images)):
            print "> Image ", i, ": %f" % images[i].timestamp
            cv2.imwrite(fnames[i], images[i].data)
