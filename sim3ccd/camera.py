#!/usr/bin/env python
# encoding: utf-8

from threading import Thread, Lock
from time import time, sleep
from dcimage import Image
from collections import deque
import cv2

__all__ = ['CameraError', 'Camera', 'CameraThreaded']


class CameraError(RuntimeError):
    pass


class Camera(object):
    def __init__(self, cam_index):
        """ Initialize the camera specified by the cam_index """
        # Just a dict with the properties accepted by the cam
        self._props = {'brigthness': 10, 'exposure': 15, 'gain': 14,
                       'fps': 5, 'saturation': 12, 'frame_width': 3,
                       'frame_height': 4, 'hue': 13, 'rectification': 18}
        self._cap = cv2.VideoCapture(cam_index)

    def set(self, propName, propVal):
        """ Set a the value for a propertie accepted by the camera """
        try:
            self._cap.set(self._props[propName], propVal)
        except KeyError:
            # The given propName does not exist
            pass

    def get(self, propName):
        """ Get the value of the especified property accepted by the camera """
        value = -1
        try:
            value = self._cap.get(self._props[propName])
        except KeyError:
            # The given propName does not exist
            pass
        return value


class CameraThreaded(Thread):
    def __init__(self, event, cam_index=None):
        Thread.__init__(self)
        if cam_index is not None:
            self._camera = Camera(cam_index)
        self._can_grab_event = event
        self._should_abort = False
        self._put_in_deque = False
        self._abort_lock = Lock()
        self._deque_lock = Lock()
        self._timing_lock = Lock()
        self._time_to_sleep = 0.0
        self._images_deque = deque()
        self._last_timestamp = -1
        self.start()

    def setCamera(self, camera=None):
        if camera is None:
            return
        self._camera = camera

    def abort(self):
        self._abort_lock.acquire()
        self._should_abort = True
        self._abort_lock.release()
        self._camera._cap.release()

    def putInDeque(self):
        self._deque_lock.acquire()
        self._put_in_deque = True
        self._deque_lock.release()

    def setTiming(self, new_time):
        self._timing_lock.acquire()
        self._time_to_sleep = new_time
        self._timing_lock.release()

    def run(self):
        while True:
            self._abort_lock.acquire()
            sa = self._should_abort
            self._abort_lock.release()

            if sa:
                break

            self._can_grab_event.wait()
            self._camera._cap.grab()
            self._last_timestamp = time()

            self._deque_lock.acquire()
            store = self._put_in_deque
            self._deque_lock.release()

            self._timing_lock.acquire()
            timing = self._time_to_sleep
            self._timing_lock.release()

            if store:
                (ret, img) = self._camera._cap.retrieve()
                _image = Image()
                _image.data = img
                _image.timestamp = self._last_timestamp
                self._images_deque.append(_image)
            sleep(timing)

    def retrieve(self):
        (ret, img) = self._camera._cap.retrieve()
        _image = Image()
        _image.data = img
        _image.timestamp = self._last_timestamp
        return _image

    def getDeque(self):
        return self._images_deque
