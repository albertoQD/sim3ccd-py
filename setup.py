#!/usr/bin/env python
# encoding: utf-8

from distutils.core import setup


setup(
    name="sim3ccd",
    version="1.0b",
    description="Simultaneous image acquisition with OpenCV",
    author="Alberto QUINTERO DELGADO",
    author_email="ajquinterod@gmail.com",
    url="https://github.com/albertoQD/sim-3ccd",

    packages=["sim3ccd"],
)
